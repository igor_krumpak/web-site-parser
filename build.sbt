name := "HtmlParserLibrary"

version := "1.0"

libraryDependencies += "net.sourceforge.htmlcleaner" % "htmlcleaner" % "2.6"

libraryDependencies += "io.spray" %%  "spray-json" % "1.3.1"

libraryDependencies += "org.scalaj" %% "scalaj-http" % "0.3.16"

libraryDependencies ++= Seq("org.slf4j" % "slf4j-api" % "1.7.5",
  "org.slf4j" % "slf4j-simple" % "1.7.5",
  "org.clapper" %% "grizzled-slf4j" % "1.0.2")


