import java.text.SimpleDateFormat
import java.util.{Calendar, TimeZone}

import grizzled.slf4j.Logger
import si.iitech.coininfo.CoinInfoParser
import si.iitech.couch.CoinDB
import si.iitech.market.{BTCe, BTER, Cryptsy}
import si.iitech.model.{Coin, CurrencyTransfer, Market}

import scala.collection.mutable.ListBuffer

//import si.iitech.model.CoinProtocol._
//import spray.json._

import spray.json._
import si.iitech.model.CoinProtocol._

/**
 * Created by Igor on 29.10.2014.
 */
object CoinParser {

  val dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm")
  val logger = Logger("CoinParser")

  def startParsing() = {

    val calendar = Calendar.getInstance()
    calendar.setTimeZone(TimeZone.getTimeZone("UTC"))
    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
    val marketData = getMartketData()
    saveCoin(marketData, "FTC", "Feathercoin", "feathercoin/", calendar)
    saveCoin(marketData, "DOGE", "Dogecoin", "dogecoin/", calendar)
    saveCoin(marketData, "LTC", "Litecoin", "litecoin/", calendar)
  }

  def getMartketData(): List[CurrencyTransfer] = {
    var values = List[CurrencyTransfer]()
    values = values ++ Cryptsy.parseWebSite()
    values = values ++ BTCe.parseWebSite()
    values = values ++ BTER.parseWebSite()
    values
  }

  def createMarketList(values: List[CurrencyTransfer]) = {
    var marketList = ListBuffer[Market]()
    for (tempValue <- values)
      marketList += Market(tempValue.market, tempValue.buyValue, tempValue.sellValue)
    if (marketList != null)
      marketList.toList
    else
      null
  }

  def createKeyFormatDate(calendar: Calendar): Array[Int] = {
    Array(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE))
  }

  def saveCoin(markets: List[CurrencyTransfer], tag: String, fullTitle: String, coinInfoPath: String, calendar: Calendar) = {
    val coinInfo = CoinInfoParser.parseCoinInfo(coinInfoPath)
    val marketEntries = createMarketList(markets.filter(x => x.tag == tag))
    val formatedDate = dateFormat.format(calendar.getTime)
    val coin = (Coin(tag, fullTitle, marketEntries, coinInfo, formatedDate, createKeyFormatDate(calendar)))
    println(coin.toJson.prettyPrint)
    if(CoinDB.save(coin.toJson.prettyPrint))
      logger.info("Coin " + coin.tag + " was saved at " + formatedDate);

  }


}
