import si.iitech.net.SecurityBypasser


object Main extends App {

  SecurityBypasser destroyAllSSLSecurityForTheEntireVMForever()

  val oneMinute = 1000 * 60
  val tenMinutes = oneMinute * 10
  val oneHour = oneMinute * 60

  while (true) {
    CoinParser.startParsing()
    Thread.sleep(tenMinutes)
  }
}