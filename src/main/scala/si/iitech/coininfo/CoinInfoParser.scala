package si.iitech.coininfo

import grizzled.slf4j.Logger
import si.iitech.model.CoinInfo
import si.iitech.net.WebSiteParser
import si.iitech.util.Util

/**
 * Created by Igor on 28.10.2014.
 */
object CoinInfoParser extends WebSiteParser {

  val logger = Logger("CoinInfoParser")
  val SOURCE = "https://bitinfocharts.com/"

  val EXTRA_SIGNS: Array[String] = Array("&#xf063;", "&#xf062;", ",")

  def removeExtraSigns(value: String): String = {
    var filter = value
    for (sign <- EXTRA_SIGNS)
      filter = filter.replaceAll(sign, "")
    filter
  }

  def parseCoinInfo(coinPath: String): CoinInfo = {
    try {
      val mainNode = readWebSite(SOURCE + coinPath)

      val currentBlockRaw = mainNode.findElementByAttValue("id", "tdid9", true, true).getText.toString
      val currentBlock = currentBlockRaw.split(" ")(0)

      val currentDifficultyRaw = mainNode.findElementByAttValue("id", "tdid14", true, true).getText.toString
      val currentDifficulty = removeExtraSigns(currentDifficultyRaw)

      val networkHashrateRaw = mainNode.findElementByAttValue("id", "tdid15", true, true).getText.toString
      val networkHashrate = removeExtraSigns(networkHashrateRaw)
      val networkHashrateValue = networkHashrate.split(" ")(0)

      val networkHashrateUnit = networkHashrate.split(" ")(1)

      val totalCoinsRaw = mainNode.findElementByAttValue("id", "tdid0", true, true).getText.toString
      val totalCoins = totalCoinsRaw.split(" ")(0)

      CoinInfo(Util.toLongFromComma(currentBlock), Util.toFloat(currentDifficulty), Util.toFloat(networkHashrateValue), networkHashrateUnit, Util.toLongFromComma(totalCoins))
    } catch {
      case e: Exception => {
        logger.error(e.toString)
        null
      }
    }

  }
}
