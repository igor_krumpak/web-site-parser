package si.iitech.couch

import grizzled.slf4j.Logger


/**
 * Created by Igor on 6.11.2014.
 */
object CoinDB extends DB {

  override def getDBTitle(): String = "coins"

  override def getDBLink(): String = "http://localhost:5984/"

  override def getLogger(): Logger = Logger("CoinInfoParser")

  override def getUsername(): String = "igor"

  override def getPassword(): String = "secret"
}
