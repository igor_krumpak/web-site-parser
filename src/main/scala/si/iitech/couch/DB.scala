package si.iitech.couch

import java.net.SocketTimeoutException
import java.util.UUID

import grizzled.slf4j.Logger

import scalaj.http.Http

/**
 * Created by Igor on 6.11.2014.
 */
trait DB {

  val logger = getLogger()
  val STATUS_CREATED = 201
  val STATUS_OK = 200

  val DB = getDBTitle()
  val DB_LINK = getDBLink()

  def checkDB(): Boolean = {
    try {
      val responseCode = Http(DB_LINK + DB).method("get").responseCode
      if (responseCode == STATUS_OK)
        true
      else
        false
    } catch {
      case e: SocketTimeoutException => {
        logger.error(e.toString)
        false
      }
      case e: Exception => {
        logger.error(e.toString)
        false
      }
    }
  }

  def createDB(): Boolean = {
    try {
      val cookie = getSessionCookie()
      if(cookie.isEmpty)
        false
      val responseCode = Http(DB_LINK + DB).method("put").
        header("Cookie", cookie).
        header("X-CouchDB-WWW-Authenticate", "Cookie").
        header("Content-Type", "application/x-www-form-urlencoded").
        responseCode
      println("db created status " + responseCode)
      if (responseCode == STATUS_CREATED)
        true
      else
        false
    } catch {
      case e: SocketTimeoutException => {
        logger.error(e.toString)
        false
      }
      case e: Exception => {
        logger.error(e.toString)
        false
      }
    }
  }

  def save(model: String): Boolean = {
    try {
      if (checkDB()) {
        val cookie = getSessionCookie()
        if(cookie.isEmpty)
          false
        val responseCode = Http.postData(DB_LINK + DB + "/" + UUID.randomUUID.toString, model).method("put").
          header("Cookie", cookie).
          header("X-CouchDB-WWW-Authenticate", "Cookie").
          header("Content-Type", "application/x-www-form-urlencoded").
          responseCode
        if (responseCode != STATUS_CREATED)
          false
        else true
      } else {
        if (createDB())
          save(model)
        else
          false
      }
    } catch {
      case e: SocketTimeoutException => {
        logger.error(e.toString)
        false
      }
      case e: Exception => {
        logger.error(e.toString)
        false
      }
    }
  }

  def getSessionCookie(): String = {
    try {
      val response= Http.post(DB_LINK + "_session").params("name" -> getUsername(), "password" -> getPassword())
      response.asCodeHeaders._2.get("Set-Cookie").get(0).split(";")(0)
    } catch {
      case e: SocketTimeoutException => {
        logger.error(e.toString)
        ""
      }
      case e: Exception => {
        logger.error(e.toString)
        ""
      }
    }
  }

  def getDBTitle(): String

  def getDBLink(): String

  def getLogger(): Logger

  def getUsername(): String

  def getPassword(): String

}



