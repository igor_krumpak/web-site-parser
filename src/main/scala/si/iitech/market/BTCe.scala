package si.iitech.market

import grizzled.slf4j.Logger
import si.iitech.model.CurrencyTransfer
import si.iitech.util.Util

import scala.collection.mutable.ListBuffer

object BTCe extends MarketParser {

  val logger = Logger("BTCe")
  val SOURCE_TITLE = "BTCe"
  val SOURCE_LTC_COIN = "https://btc-e.com/exchange/ltc_btc"
  val SOURCE_FTC_COIN = "https://btc-e.com/exchange/ftc_btc"

  def parseWebSite(): List[CurrencyTransfer] = {
    val values = ListBuffer[CurrencyTransfer]()
    getCurrencyData(SOURCE_LTC_COIN, LTC, values)
    getCurrencyData(SOURCE_FTC_COIN, FTC, values)
    values.toList
  }

  def getCurrencyData(source: String, tag: String, values: ListBuffer[CurrencyTransfer]) = {
    try {
      val mainNode = readWebSite(source)
      val minValue = Util.formatToFloat(mainNode.findElementByAttValue("id", "min_price", true, true).getText.toString)
      val maxValue = Util.formatToFloat(mainNode.findElementByAttValue("id", "max_price", true, true).getText.toString)
      values += CurrencyTransfer(tag, SOURCE_TITLE, Util.toFloat(minValue), Util.toFloat(maxValue))
    } catch {
      case e: Exception => logger.error(e.toString)
    }
  }
}