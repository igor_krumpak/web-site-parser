package si.iitech.market

import grizzled.slf4j.Logger
import si.iitech.model.CurrencyTransfer
import si.iitech.util.Util

import scala.collection.mutable.ListBuffer

object BTER extends MarketParser {

  val logger = Logger("BTER")
  val SOURCE_TITLE = "BTER"
  val SOURCE_LTC_COIN = "https://bter.com/trade/ltc_btc"
  val SOURCE_DOGE_COIN = "https://bter.com/trade/doge_btc"
  val SOURCE_FEATHER_COIN = "https://bter.com/trade/ftc_btc"
  val SOURCE_VTC_COIN = "https://bter.com/trade/vtc_btc"
  val SOURCE_DRK_COIN = "https://bter.com/trade/drk_btc"

  def parseWebSite(): List[CurrencyTransfer] = {
    val values = ListBuffer[CurrencyTransfer]()
    getCurrencyData(SOURCE_LTC_COIN, LTC, values)
    getCurrencyData(SOURCE_DOGE_COIN, DOGE, values)
    getCurrencyData(SOURCE_FEATHER_COIN, FTC, values)
    getCurrencyData(SOURCE_VTC_COIN, VTC, values)
    getCurrencyData(SOURCE_DRK_COIN, DRK, values)
    values.toList
  }

  def getCurrencyData(source: String, tag: String, values: ListBuffer[CurrencyTransfer]) = {
    try {
      val mainNode = readWebSite(source)
      val tradingNode = mainNode.findElementByAttValue("class", "trading", true, true)
      val dtNodes = tradingNode.getElementsByName("dt", true)
      values += CurrencyTransfer(tag, SOURCE_TITLE, Util.toFloat(dtNodes(2).getText.toString), Util.toFloat(dtNodes(1).getText.toString))
    } catch {
      case e: Exception => logger.error(e.toString)
    }
  }
}