package si.iitech.market

import grizzled.slf4j.Logger
import si.iitech.model.CurrencyTransfer
import si.iitech.util.Util

import scala.collection.mutable.ListBuffer

object Cryptsy extends MarketParser {

  val logger = Logger("Cryptsy")
  val SOURCE_TITLE = "Cryptsy"
  val SOURCE = "https://www.cryptsy.com/"
  val TAG_LTC = 3
  val TAG_FTC = 5
  val TAG_DOGE = 132
  val TAG_VTC = 151
  val TAG_DARK = 155


  def parseWebSite(): List[CurrencyTransfer] = {
    val values = ListBuffer[CurrencyTransfer]()
    getCurrencyData(TAG_LTC, LTC, values)
    getCurrencyData(TAG_FTC, FTC, values)
    getCurrencyData(TAG_DOGE, DOGE, values)
    getCurrencyData(TAG_VTC, VTC, values)
    getCurrencyData(TAG_DARK, DRK, values)
    values.toList
  }

  def getCurrencyData(id: Integer, tag: String, values: ListBuffer[CurrencyTransfer]) = {
    try {
      val mainNode = readWebSite(SOURCE)
      val marketTableNode = mainNode.findElementByAttValue("id", "markettable", true, true)
      val tr = marketTableNode.findElementByAttValue("class", "class_market_price_" + id, true, true).getParent.getParent
      val maxValue = Util.formatToFloat(tr.getChildTagList.get(4).getText.toString)
      val minValue = Util.formatToFloat(tr.getChildTagList.get(5).getText.toString)
      values += CurrencyTransfer(tag, SOURCE_TITLE, Util.toFloat(minValue), Util.toFloat(maxValue))
    } catch {
      case e: Exception => logger.error(e.toString)

    }
  }
}