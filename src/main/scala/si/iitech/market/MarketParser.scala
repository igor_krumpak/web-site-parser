package si.iitech.market

import si.iitech.model.CurrencyTransfer
import si.iitech.net.WebSiteParser

/**
 * Created by Igor on 28.10.2014.
 */
abstract class MarketParser extends WebSiteParser {

  val LTC = "LTC"
  val FTC = "FTC"
  val DOGE = "DOGE"
  val VTC = "VTC"
  val DRK = "DRK"

  def parseWebSite(): List[CurrencyTransfer]
}
