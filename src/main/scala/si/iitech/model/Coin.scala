package si.iitech.model

/**
 * Created by Igor on 9.11.2014.
 */
case class Coin(tag: String, title: String, markets: List[Market], info: CoinInfo, date: String, dateKey: Array[Int])
