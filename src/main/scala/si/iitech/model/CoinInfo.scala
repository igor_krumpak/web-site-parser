package si.iitech.model

/**
 * Created by Igor on 9.11.2014.
 */
case class CoinInfo(currentBlock: Long, currentDificulty: Float, networkHashrate: Float,  networkHashrateUnit: String, totalCoins: Long)
