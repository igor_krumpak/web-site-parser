package si.iitech.model

import spray.json.DefaultJsonProtocol

/**
 * Created by Igor on 9.11.2014.
 */
object CoinProtocol extends DefaultJsonProtocol {
  implicit val marketFormat = jsonFormat3(Market)
  implicit val coinInfoFormat = jsonFormat5(CoinInfo)
  implicit val coinFormat = jsonFormat6(Coin)
}
