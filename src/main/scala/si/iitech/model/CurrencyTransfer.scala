package si.iitech.model

/**
 * Created by Igor on 9.11.2014.
 */
case class CurrencyTransfer(tag: String, market: String,  buyValue: Float, sellValue: Float)
