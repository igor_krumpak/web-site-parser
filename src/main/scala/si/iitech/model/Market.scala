package si.iitech.model

/**
 * Created by Igor on 9.11.2014.
 */
case class Market(title: String, buyValue: Float, sellValue: Float)
