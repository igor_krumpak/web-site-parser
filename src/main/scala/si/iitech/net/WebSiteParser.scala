package si.iitech.net

import java.net.URL

import org.htmlcleaner.{HtmlCleaner, TagNode}

trait WebSiteParser {

  def readWebSite(source: String): TagNode = {
    val cleaner = new HtmlCleaner
    val url = new URL(source)
    val connection = url.openConnection()
    connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.120 Safari/535.2")
    val rootNode = cleaner.clean(connection.getInputStream)
    rootNode
  }


}

//https://btc-e.com/exchange/
//https://bter.com/trade/aur_btc
//https://www.swisscex.com/market/DOGE_BTC
//