package si.iitech.util

object Util {

  def formatToFloat(text: String) = {
    text.replaceAll("[^\\d.]", "")
  }

  def toFloat(value: String): Float = {
    try {
      value.toFloat
    } catch {
      case e: Exception => 0f
    }
  }

  def toLongFromComma(value: String): Long = {
    try {
      value.replace(",", "").toLong
    } catch {
      case e: Exception => 0L
    }
  }


}
